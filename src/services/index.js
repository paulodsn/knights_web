import knights from "./knights.service"
import weapons from "./weapons.service"

export default {
  knights,
  weapons
}