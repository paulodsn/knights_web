import { createWebHistory, createRouter } from "vue-router";

const routes = [
  {
    path: "/",
    redirect: (to) => {
      return { path: "/knights" };
    },
  },
  {
    path: "/knights",
    name: "knights-list",
    component: () => import("./../views/knight/list/List.vue"),
  },
  {
    path: "/knights/create",
    name: "knights-create",
    component: () => import("./../views/knight/create/Create.vue"),
  },
  {
    path: "/knights/:id",
    name: "knights-edit",
    component: () => import("./../views/knight/edit/Edit.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
