import http from "./api";

export default {
  list(filter) {
    return http.get("/knights", {
      params: {
        filter: filter ? 'heroes' : undefined
      }
    });
  },
  get(id) {
    return http.get(`/knights/${id}`);
  },
  update(id, data) {
    return http.patch(`/knights/${id}`, data);
  },
  create(data) {
    return http.post("/knights", data);
  },
  delete(id) {
    return http.delete(`/knights/${id}`);
  }
}