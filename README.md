# Knights Web

## Pré-Requisito

- Node 18+

## Como executar

1. Execute `pnpm install`
2. Execute o comando `pnpm dev` para iniciar o projeto
3. pronto :)

### Com Docker

- Execute o comando: `docker compose up`
- Acesse http://localhost:8080
